# `manosphrames`

Repository for the paper:

> Sara Gemelli & Gosse Minnema. "Manosphrames: exploring an Italian incel community through the lens of NLP and Frame Semantics". To be published in: _1st Workshop on Reference, Framing, and Perspective_ (LREC-COLING, Torino 2024). 

We are currently updating our scraping code due to changes in the forum website that were made after the conclusion of our experiments. In the meantime, feel free to email Gosse (see the paper for contact details) to request the corpus, explaining who you are and your intended (research) purpose.